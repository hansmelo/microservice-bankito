package com.hans.bankito.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hans.bankito.JavalinApp;
import com.hans.bankito.account.domain.CreateAccountJson;
import com.hans.bankito.transaction.TransactionService;
import com.hans.bankito.transaction.domain.CreateTransactionJson;
import com.hans.bankito.transaction.domain.TransactionStatus;
import com.hans.bankito.util.MultithreadedStressTester;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.javalin.Javalin;
import org.json.JSONObject;
import org.junit.*;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.Assert.*;


public class BankitoApplicationTest {

    private static final int PORT = 9000;
    private static final String ALIVE_RESPONSE = "I am alive!";
    private static final int OK_STATUS = 200;
    private static final int CREATED_STATUS = 201;
    public static final String ACCEPT = "accept";
    public static final String APPLICATION_JSON = "application/json";
    private static String URL = "http://localhost:" + PORT + "/bankito-api";
    private static final String ALIVE_URL = URL + "/alive";
    private static final String CREATE_ACCOUNT_URL = URL + "/account";
    private static final String TRANSFER_URL = URL + "/transfer";

    private static Javalin app;

    @Before
    public void setup() {
        app = new JavalinApp(PORT).init();

        Unirest.setObjectMapper(new ObjectMapper() {
            com.fasterxml.jackson.databind.ObjectMapper mapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public String writeValue(Object value) {
                try {
                    return mapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                return "";
            }

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return mapper.readValue(value, valueType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
    }

    @After
    public void shutdown() throws InterruptedException {
        Thread.sleep(1000);
        app.stop();
    }

    @Test
    public void testAppIsAlive() throws UnirestException {
        HttpResponse<String> response = Unirest.get(ALIVE_URL).asString();
        assertNotNull(response);
        assertEquals(OK_STATUS, response.getStatus());
        assertEquals(ALIVE_RESPONSE, response.getBody());
    }

    @Test(expected = UnirestException.class)
    public void testAppIsNotAlive() throws UnirestException {
        app.stop();
        Unirest.get(ALIVE_URL).asString();
    }

    @Test
    public void testCreateAccount() throws UnirestException {
        CreateAccountJson createAccountJson = new CreateAccountJson(20.0);
        HttpResponse<JsonNode> jsonResponse = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson)
                .asJson();
        assertNotNull(jsonResponse);
        assertEquals(CREATED_STATUS, jsonResponse.getStatus());
        assertNotNull(jsonResponse.getBody().getObject().get("id"));
        assertEquals(20.0, jsonResponse.getBody().getObject().get("amount"));


        CreateAccountJson createAccountJson2 = new CreateAccountJson(50.0);
        HttpResponse<JsonNode> jsonResponse2 = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson2)
                .asJson();
        assertEquals(CREATED_STATUS, jsonResponse2.getStatus());
        assertNotEquals(jsonResponse.getBody().getObject().get("id"),
                jsonResponse2.getBody().getObject().get("id"));
        assertEquals(50.0, jsonResponse2.getBody().getObject().get("amount"));
    }

    @Test
    public void testTranfer() throws UnirestException {
        CreateAccountJson createAccountJson = new CreateAccountJson(30.0);
        HttpResponse<JsonNode> jsonResponse = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson)
                .asJson();

        Integer senderAccountId = (Integer) jsonResponse.getBody().getObject().get("id");

        CreateAccountJson createAccountJson2 = new CreateAccountJson(50.0);
        HttpResponse<JsonNode> jsonResponse2 = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson2)
                .asJson();

        Integer receiverAccountId = (Integer) jsonResponse2.getBody().getObject().get("id");

        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccountId, receiverAccountId, 10.0);
        String completeTransferUrl = TRANSFER_URL;
        HttpResponse<JsonNode> jsonTransfer = Unirest.post(completeTransferUrl)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createTransactionJson)
                .asJson();
        JSONObject transfer = jsonTransfer.getBody().getObject();
        JSONObject senderAccount = (JSONObject) transfer.get("senderAccount");
        JSONObject receiverAccount = (JSONObject) transfer.get("receiverAccount");

        assertEquals(CREATED_STATUS, jsonTransfer.getStatus());
        assertNotNull(transfer.get("id"));
        assertEquals(new Double(10.0), transfer.get("valueOperation"));
        assertEquals(new Double(20.0), senderAccount.get("amount"));
        assertEquals(new Double(60.0), receiverAccount.get("amount"));
        assertEquals(TransactionStatus.SUCCESS.toString(), transfer.get("status"));
        assertEquals(TransactionService.TRANSACTION_IS_DONE, transfer.get("msg"));
    }

    @Test
    public void testTranferThreads() throws UnirestException, InterruptedException {
        CreateAccountJson createAccountJson = new CreateAccountJson(10000.0);
        HttpResponse<JsonNode> jsonResponse = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson)
                .asJson();

        Integer senderAccountId = (Integer) jsonResponse.getBody().getObject().get("id");

        CreateAccountJson createAccountJson2 = new CreateAccountJson(0.0);
        HttpResponse<JsonNode> jsonResponse2 = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson2)
                .asJson();

        Integer receiverAccountId = (Integer) jsonResponse2.getBody().getObject().get("id");

        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccountId, receiverAccountId, 1.0);
        String completeTransferUrl = TRANSFER_URL;

        ConcurrentLinkedQueue<HttpResponse<JsonNode>> concurrentLinkedQueue = new ConcurrentLinkedQueue();

        MultithreadedStressTester stressTester = new MultithreadedStressTester(10, 1000);
        stressTester.stress(() ->
                Unirest.post(completeTransferUrl)
                        .header(ACCEPT, APPLICATION_JSON)
                        .body(createTransactionJson)
                        .asJsonAsync(new Callback<JsonNode>() {
                            public void failed(UnirestException e) {
                            }
                            public void completed(HttpResponse<JsonNode> response) {
                                concurrentLinkedQueue.add(response);
                            }
                            public void cancelled() {
                            }

                        })
        );
        stressTester.shutdown();
        System.out.println("Transaction Requests With Success " + concurrentLinkedQueue.size());
        concurrentLinkedQueue.forEach(jsonTransfer -> {
            JSONObject transfer = jsonTransfer.getBody().getObject();
            JSONObject senderAccount = (JSONObject) transfer.get("senderAccount");
            JSONObject receiverAccount = (JSONObject) transfer.get("receiverAccount");

            assertEquals(CREATED_STATUS, jsonTransfer.getStatus());
            assertNotNull(transfer.get("id"));
            assertEquals(new Double(1.0), transfer.get("valueOperation"));
            assertEquals(new Double(10000.0), new Double((Double) senderAccount.get("amount") + (Double) receiverAccount.get("amount")));
            assertEquals(TransactionStatus.SUCCESS.toString(), transfer.get("status"));
            assertEquals(TransactionService.TRANSACTION_IS_DONE, transfer.get("msg"));
        });
    }

    @Test
    public void testTranferInsufficientBalance() throws UnirestException {
        CreateAccountJson createAccountJson = new CreateAccountJson(30.0);
        HttpResponse<JsonNode> jsonResponse = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson)
                .asJson();

        Integer senderAccountId = (Integer) jsonResponse.getBody().getObject().get("id");

        CreateAccountJson createAccountJson2 = new CreateAccountJson(50.0);
        HttpResponse<JsonNode> jsonResponse2 = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson2)
                .asJson();

        Integer receiverAccountId = (Integer) jsonResponse2.getBody().getObject().get("id");

        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccountId, receiverAccountId, 31.0);
        String completeTransferUrl = TRANSFER_URL;
        HttpResponse<JsonNode> jsonTransfer = Unirest.post(completeTransferUrl)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createTransactionJson)
                .asJson();
        JSONObject transfer = jsonTransfer.getBody().getObject();
        JSONObject senderAccount = (JSONObject) transfer.get("senderAccount");
        JSONObject receiverAccount = (JSONObject) transfer.get("receiverAccount");

        assertEquals(500, jsonTransfer.getStatus());
        assertNotNull(transfer.get("id"));
        assertEquals(new Double(31.0), transfer.get("valueOperation"));
        assertEquals(new Double(30.0), senderAccount.get("amount"));
        assertEquals(new Double(50.0), receiverAccount.get("amount"));
        assertEquals(TransactionStatus.FAILURE.toString(), transfer.get("status"));
        assertEquals(TransactionService.INSUFFICIENT_BALANCE, transfer.get("msg"));
    }

    @Test
    public void testTranferNotFound() throws UnirestException {
        CreateAccountJson createAccountJson = new CreateAccountJson(30.0);
        HttpResponse<JsonNode> jsonResponse = Unirest.post(CREATE_ACCOUNT_URL)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createAccountJson)
                .asJson();

        Integer senderAccountId = (Integer) jsonResponse.getBody().getObject().get("id");

        Integer receiverAccountId = new Random().nextInt();

        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccountId, receiverAccountId, 20.0);
        String completeTransferUrl = TRANSFER_URL;
        HttpResponse<JsonNode> jsonTransfer = Unirest.post(completeTransferUrl)
                .header(ACCEPT, APPLICATION_JSON)
                .body(createTransactionJson)
                .asJson();
        JSONObject transfer = jsonTransfer.getBody().getObject();
        JSONObject senderAccount = (JSONObject) transfer.get("senderAccount");
        JSONObject receiverAccount = (JSONObject) transfer.get("receiverAccount");

        assertEquals(500, jsonTransfer.getStatus());
        assertNotNull(transfer.get("id"));
        assertEquals(new Double(20.0), transfer.get("valueOperation"));
        assertEquals(TransactionStatus.FAILURE.toString(), transfer.get("status"));
        assertEquals(TransactionService.ACCOUNT_IS_NOT_FOUND, transfer.get("msg"));
    }


}
