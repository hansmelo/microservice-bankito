package com.hans.bankito.transaction;

import com.hans.bankito.account.AccountService;
import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.exception.NotFoundAccountException;
import com.hans.bankito.transaction.domain.CreateTransactionJson;
import com.hans.bankito.transaction.domain.TransactionStatus;
import com.hans.bankito.transaction.domain.model.Transaction;
import com.hans.bankito.util.MultithreadedStressTester;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionTest {

    public static final double AMOUNT_SENDER = 20.0;
    public static final double AMOUNT_RECEIVER = 20.0;
    public static final double AMOUT_TRANSACTION = 10.0;
    TransactionService transactionService;
    AccountService accountService = mock(AccountService.class);

    @Before
    public void setup() {
        this.transactionService = new TransactionService(accountService);
    }

    @Test
    public void testTransfer() throws NotFoundAccountException {
        Account senderAccount = new Account(1, AMOUNT_SENDER);
        Account receiverAccount = new Account(2, AMOUNT_RECEIVER);
        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccount.getId(), receiverAccount.getId(), AMOUT_TRANSACTION);
        when(accountService.getAccount(senderAccount.getId())).thenReturn(senderAccount);
        when(accountService.getAccount(receiverAccount.getId())).thenReturn(receiverAccount);

        Transaction transaction = transactionService.transfer(createTransactionJson);

        assertNotNull(transaction);
        assertEquals(new Double(AMOUNT_SENDER - AMOUT_TRANSACTION), transaction.getSenderAccount().getAmount());
        assertEquals(new Double(AMOUNT_RECEIVER + AMOUT_TRANSACTION), transaction.getReceiverAccount().getAmount());

        Transaction transaction2 = transactionService.transfer(createTransactionJson);

        assertNotNull(transaction2);
        assertEquals(new Double(AMOUNT_SENDER - AMOUT_TRANSACTION - AMOUT_TRANSACTION), transaction2.getSenderAccount().getAmount());
        assertEquals(new Double(AMOUNT_RECEIVER + AMOUT_TRANSACTION + AMOUT_TRANSACTION), transaction2.getReceiverAccount().getAmount());

    }

    @Test
    public void testTransferFromMultipleThreadsSimultaneously()
            throws InterruptedException, NotFoundAccountException {
        MultithreadedStressTester stressTester = new MultithreadedStressTester(10, 1000);
        Account senderAccount = new Account(1, 10000.0);
        Account receiverAccount = new Account(2, 0.0);
        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccount.getId(), receiverAccount.getId(), 1.0);
        when(accountService.getAccount(senderAccount.getId())).thenReturn(senderAccount);
        when(accountService.getAccount(receiverAccount.getId())).thenReturn(receiverAccount);

        ConcurrentHashMap<UUID, Transaction> transactions = new ConcurrentHashMap<>();
        stressTester.stress(() -> {
            Transaction transaction = transactionService.transfer(createTransactionJson);
            transactions.putIfAbsent(transaction.getId(), transaction);
        });
        stressTester.shutdown();

        assertEquals(10000, transactions.size());
        transactions.forEach((uuid, transaction) -> {
                    assertEquals(new Double(10000.0),
                            new Double(transaction.getSenderAccount().getAmount() + transaction.getReceiverAccount().getAmount()));
                    assertEquals(TransactionStatus.SUCCESS, transaction.getStatus());
                    assertEquals(TransactionService.TRANSACTION_IS_DONE, transaction.getMsg());
                    assertEquals(new Double(0.0), senderAccount.getAmount());
                    assertEquals(new Double(10000.0), receiverAccount.getAmount());
                }
        );
    }

    @Test
    public void testTransferFromMultipleThreadsSimultaneouslyForceInsufficientBalance()
            throws InterruptedException, NotFoundAccountException {
        MultithreadedStressTester stressTester = new MultithreadedStressTester(10, 1000);
        Account senderAccount = new Account(1, 9999.0);
        Account receiverAccount = new Account(2, 0.0);
        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccount.getId(), receiverAccount.getId(), 1.0);
        when(accountService.getAccount(senderAccount.getId())).thenReturn(senderAccount);
        when(accountService.getAccount(receiverAccount.getId())).thenReturn(receiverAccount);

        ConcurrentHashMap<UUID, Transaction> transactions = new ConcurrentHashMap<>();
        stressTester.stress(() -> {
            Transaction transaction = transactionService.transfer(createTransactionJson);
            transactions.putIfAbsent(transaction.getId(), transaction);
        });
        stressTester.shutdown();

        assertEquals(10000, transactions.size());
        transactions.entrySet().stream()
                .filter(uuidTransactionEntry -> uuidTransactionEntry.getValue().getStatus().equals(TransactionStatus.FAILURE))
                .forEach(transaction -> {
                    assertEquals(TransactionService.INSUFFICIENT_BALANCE, transaction.getValue().getMsg());
                    assertEquals(new Double(0.0), senderAccount.getAmount());
                    assertEquals(new Double(9999.0), receiverAccount.getAmount());
                    assertEquals(TransactionStatus.FAILURE, transaction.getValue().getStatus());
                });

        transactions.entrySet().stream()
                .filter(uuidTransactionEntry -> !uuidTransactionEntry.getValue().getStatus().equals(TransactionStatus.FAILURE))
                .forEach(transaction -> {
                    assertEquals(new Double(9999.0),
                            new Double(transaction.getValue().getSenderAccount().getAmount() + transaction.getValue().getReceiverAccount().getAmount()));
                    assertEquals(TransactionService.TRANSACTION_IS_DONE, transaction.getValue().getMsg());
                    assertEquals(TransactionStatus.SUCCESS, transaction.getValue().getStatus());
                });
    }

    @Test
    public void testTransferFromMultipleThreadsSimultaneouslyForceAccountNotFound()
            throws InterruptedException, NotFoundAccountException {
        MultithreadedStressTester stressTester = new MultithreadedStressTester(10, 1000);
        Account senderAccount = new Account(1, 10000.0);
        Account receiverAccount = new Account(2, 0.0);
        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccount.getId(), receiverAccount.getId(), 1.0);
        when(accountService.getAccount(senderAccount.getId())).thenReturn(senderAccount);
        when(accountService.getAccount(receiverAccount.getId())).thenThrow(NotFoundAccountException.class);

        ConcurrentHashMap<UUID, Transaction> transactions = new ConcurrentHashMap<>();
        stressTester.stress(() -> {
            Transaction transaction = transactionService.transfer(createTransactionJson);
            transactions.putIfAbsent(transaction.getId(), transaction);
        });
        stressTester.shutdown();

        assertEquals(10000, transactions.size());
        transactions.forEach((uuid, transaction) -> {
                    assertEquals(TransactionStatus.FAILURE, transaction.getStatus());
                    assertEquals(TransactionService.ACCOUNT_IS_NOT_FOUND, transaction.getMsg());
                    assertEquals(new Double(10000.0), senderAccount.getAmount());
                }
        );
    }

    @Test
    public void testTransferFromMultipleThreadsSimultaneouslyForceException()
            throws InterruptedException, NotFoundAccountException {
        MultithreadedStressTester stressTester = new MultithreadedStressTester(10, 1000);
        Account senderAccount = new Account(1, 10000.0);
        Account receiverAccount = new Account(2, 0.0);
        CreateTransactionJson createTransactionJson = new CreateTransactionJson(senderAccount.getId(), receiverAccount.getId(), 1.0);
        when(accountService.getAccount(senderAccount.getId())).thenReturn(senderAccount);
        when(accountService.getAccount(receiverAccount.getId())).thenAnswer(invocation -> {
            throw new Exception();
        });

        ConcurrentHashMap<UUID, Transaction> transactions = new ConcurrentHashMap<>();
        stressTester.stress(() -> {
            Transaction transaction = transactionService.transfer(createTransactionJson);
            transactions.putIfAbsent(transaction.getId(), transaction);
        });
        stressTester.shutdown();

        assertEquals(10000, transactions.size());
        transactions.forEach((uuid, transaction) -> {
                    assertEquals(TransactionStatus.FAILURE, transaction.getStatus());
                    assertEquals(TransactionService.UNEXPECTED_ERROR, transaction.getMsg());
                    assertEquals(new Double(10000.0), senderAccount.getAmount());
                }
        );
    }
}
