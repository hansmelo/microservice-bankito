package com.hans.bankito.account;

import com.hans.bankito.account.domain.CreateAccountJson;
import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.exception.NotFoundAccountException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountServiceTest {

    private AccountService accountService;

    private AccountRepository accountRepository = mock(AccountRepository.class);

    @Before
    public void setup() {
        accountService = new AccountService(accountRepository);
    }

    @Test
    public void testCreateAccount() {
        CreateAccountJson createAccountJson = new CreateAccountJson(10.0);
        when(accountRepository.save(any(Account.class))).thenReturn(new Account(1, createAccountJson.getAmount()));

        Account account = accountService.createAccount(createAccountJson);

        assertNotNull(account.getId());
        assertEquals(createAccountJson.getAmount(), account.getAmount());
    }

    @Test
    public void getAccount() throws NotFoundAccountException {
        CreateAccountJson createAccountJson = new CreateAccountJson(10.0);
        when(accountRepository.save(any(Account.class))).thenReturn(new Account(1, createAccountJson.getAmount()));
        Account createdAccount = accountService.createAccount(createAccountJson);

        when(accountRepository.find(any(Integer.class))).thenReturn(new Account(1, createAccountJson.getAmount()));
        Account retrievedAccount = accountService.getAccount(createdAccount.getId());

        assertEquals(createdAccount.getId(), retrievedAccount.getId());
        assertEquals(createdAccount.getAmount(), retrievedAccount.getAmount());
    }

    @Test(expected = NotFoundAccountException.class)
    public void getNotFoundAccount() throws NotFoundAccountException {
        when(accountRepository.find(any(Integer.class))).thenThrow(NotFoundAccountException.class);
        accountService.getAccount(10000);
    }
}
