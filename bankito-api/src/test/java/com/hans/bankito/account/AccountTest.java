package com.hans.bankito.account;

import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.exception.InsufficientBalance;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AccountTest {

    @Test
    public void testCreateAccountWithIdAndAmount() {
        Account account = new Account(1, 10.0);
        assertEquals(new Integer(1), account.getId());
        assertEquals(new Double(10.0), account.getAmount());
    }

    @Test
    public void testCreateAccountWithAmount() {
        Account account = new Account(10.0);
        assertNull(account.getId());
        assertEquals(new Double(10.0), account.getAmount());
    }

    @Test
    public void testDeposit() {
        Account account = new Account(1, 10.0);
        account.deposit(300.0);
        assertEquals(new Double(310.0), account.getAmount());
    }

    @Test
    public void testWithdrawl() throws InsufficientBalance {
        Account account = new Account(1, 100.0);
        account.withdrawal(60.0);
        assertEquals(new Double(40.0), account.getAmount());
    }

    @Test(expected = InsufficientBalance.class)
    public void testWithdrawlThrowInsufficientBalanceIfNegativeAmount() throws InsufficientBalance {
        Account account = new Account(1, 10.0);
        account.withdrawal(60.0);
    }


}
