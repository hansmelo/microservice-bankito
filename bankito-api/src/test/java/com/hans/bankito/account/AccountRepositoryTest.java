package com.hans.bankito.account;

import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.exception.NotFoundAccountException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccountRepositoryTest {

    private AccountRepository accountRepository;

    @Before
    public void setup() {
        accountRepository = AccountRepository.getInstance();
    }

    @Test
    public void testSaveAccount() {
        Account account = new Account(20.0);
        Account createdAccount = accountRepository.save(account);

        assertEquals(account.getId(), createdAccount.getId());
        assertEquals(account.getAmount(), createdAccount.getAmount());
    }

    @Test
    public void testFindAccount() throws NotFoundAccountException {
        Account account = new Account(20.0);
        Account createdAccount = accountRepository.save(account);

        Account retrievedAccount = accountRepository.find(createdAccount.getId());

        assertEquals(createdAccount.getId(), retrievedAccount.getId());
        assertEquals(createdAccount.getAmount(), retrievedAccount.getAmount());

    }

    @Test(expected = NotFoundAccountException.class)
    public void testNotFoundAccount() throws NotFoundAccountException {
        accountRepository.find(1000);
    }
}
