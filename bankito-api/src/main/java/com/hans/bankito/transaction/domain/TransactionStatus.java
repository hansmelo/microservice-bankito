package com.hans.bankito.transaction.domain;

public enum TransactionStatus {
    SUCCESS,
    FAILURE
}
