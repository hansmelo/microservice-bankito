package com.hans.bankito.transaction.domain;

public class CreateTransactionJson {

    private Integer senderAccountId;
    private Integer reiceverAccountId;
    private Double amount;

    public CreateTransactionJson() {
    }

    public CreateTransactionJson(Integer senderAccountId, Integer reiceverAccountId, Double amount) {
        this.senderAccountId = senderAccountId;
        this.reiceverAccountId = reiceverAccountId;
        this.amount = amount;
    }

    public Integer getSenderAccountId() {
        return senderAccountId;
    }

    public Integer getReiceverAccountId() {
        return reiceverAccountId;
    }

    public Double getAmount() {
        return amount;
    }
}
