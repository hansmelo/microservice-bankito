package com.hans.bankito.transaction;

import com.hans.bankito.transaction.domain.CreateTransactionJson;
import com.hans.bankito.transaction.domain.TransactionStatus;
import com.hans.bankito.transaction.domain.model.Transaction;
import io.javalin.Handler;

public class TransactionController {

    TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public Handler transfer = ctx -> {
        CreateTransactionJson transactionJson = ctx.bodyAsClass(CreateTransactionJson.class);
        Transaction transaction = transactionService.transfer(transactionJson);
        ctx.json(transaction);
        if (TransactionStatus.SUCCESS.equals(transaction.getStatus())) {
            ctx.status(201);
        } else {
            ctx.status(500);
        }
    };
}
