package com.hans.bankito.transaction.domain.model;

import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.transaction.domain.TransactionStatus;

import java.util.UUID;

public class Transaction {

    private UUID id;
    private Account senderAccount;
    private Account receiverAccount;
    private Double valueOperation;
    private TransactionStatus status;
    private String msg;

    public Transaction(Integer senderId, Integer receiverId, Double valueOperation, TransactionStatus status, String msg) {
        this.id = UUID.randomUUID();
        this.senderAccount = new Account(senderId);
        this.receiverAccount = new Account(receiverId);
        this.valueOperation = valueOperation;
        this.status = status;
        this.msg = msg;

    }

    public Transaction(Account senderAccount, Account receiverAccount, Double valueOperation, TransactionStatus status, String msg) {
        this.id = UUID.randomUUID();
        this.senderAccount = new Account(senderAccount.getId(), senderAccount.getAmount());
        this.receiverAccount = new Account(receiverAccount.getId(), receiverAccount.getAmount());
        this.valueOperation = valueOperation;
        this.status = status;
        this.msg = msg;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Account getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(Account senderAccount) {
        this.senderAccount = senderAccount;
    }

    public Account getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(Account receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public Double getValueOperation() {
        return valueOperation;
    }

    public void setValueOperation(Double valueOperation) {
        this.valueOperation = valueOperation;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
