package com.hans.bankito.transaction;

import com.hans.bankito.account.AccountService;
import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.exception.InsufficientBalance;
import com.hans.bankito.exception.NotFoundAccountException;
import com.hans.bankito.transaction.domain.CreateTransactionJson;
import com.hans.bankito.transaction.domain.TransactionStatus;
import com.hans.bankito.transaction.domain.model.Transaction;

public class TransactionService {

    public static final String TRANSACTION_IS_DONE = "Transaction is done!";
    public static final String INSUFFICIENT_BALANCE = "Insufficient Balance!";
    public static final String UNEXPECTED_ERROR = "Unexpected error!";
    public static final String ACCOUNT_IS_NOT_FOUND = "Account is not found";
    AccountService accountService;

    public TransactionService(AccountService accountService) {
        this.accountService = accountService;
    }

    //http://web.mit.edu/6.031/www/fa17/classes/21-locks/#deadlock_solution_1_lock_ordering
    public Transaction transfer(CreateTransactionJson transactionJson) {
        Account senderAccount = null;
        Account receiverAccount = null;
        Double amount = null;
        try {
            amount = transactionJson.getAmount();
            senderAccount = accountService.getAccount(transactionJson.getSenderAccountId());
            receiverAccount = accountService.getAccount(transactionJson.getReiceverAccountId());
            Object lockFirst;
            Object lockSecond;
            if (transactionJson.getSenderAccountId() < transactionJson.getReiceverAccountId()) {
                lockFirst = senderAccount;
                lockSecond = receiverAccount;
            } else {
                lockFirst = receiverAccount;
                lockSecond = senderAccount;
            }

            synchronized (lockFirst) {
                synchronized (lockSecond) {
                    senderAccount.withdrawal(amount);
                    receiverAccount.deposit(amount);
                    return new Transaction(senderAccount, receiverAccount, amount, TransactionStatus.SUCCESS, TRANSACTION_IS_DONE);

                }
            }
        } catch (InsufficientBalance ex) {
            return new Transaction(senderAccount, receiverAccount, amount, TransactionStatus.FAILURE, INSUFFICIENT_BALANCE);
        } catch (NotFoundAccountException ex) {
            return new Transaction(transactionJson.getSenderAccountId(), transactionJson.getReiceverAccountId(),
                    amount, TransactionStatus.FAILURE, ACCOUNT_IS_NOT_FOUND);
        } catch (Exception ex) {
            return new Transaction(transactionJson.getSenderAccountId(), transactionJson.getReiceverAccountId(),
                    amount, TransactionStatus.FAILURE, UNEXPECTED_ERROR);
        }

    }
}
