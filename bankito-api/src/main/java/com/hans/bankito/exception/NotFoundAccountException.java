package com.hans.bankito.exception;

public class NotFoundAccountException extends Exception {
    public NotFoundAccountException(String message) {
        super(message);
    }
}
