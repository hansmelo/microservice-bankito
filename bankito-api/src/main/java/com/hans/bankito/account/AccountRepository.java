package com.hans.bankito.account;

import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.exception.NotFoundAccountException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class AccountRepository {

    private static AccountRepository instance;

    private static Map<Integer, Account> accounts;

    private AtomicInteger accountIdGenerator;


    static {
        synchronized (AccountRepository.class) {
            if (AccountRepository.instance == null) {
                AccountRepository.instance = new AccountRepository();
            }
        }
    }

    private AccountRepository() {
        accounts = new ConcurrentHashMap<>();
        accountIdGenerator = new AtomicInteger(1);
    }

    public static AccountRepository getInstance() {
        return instance;
    }

    public Account save(Account account) {
        account.setId(accountIdGenerator.getAndIncrement());
        accounts.putIfAbsent(account.getId(), account);
        return account;
    }


    public Account find(int accountId) throws NotFoundAccountException {
        Account account = accounts.get(accountId);
        if (account == null) throw new NotFoundAccountException("Account is not found!");
        return account;
    }
}
