package com.hans.bankito.account;

import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.account.domain.CreateAccountJson;
import io.javalin.Handler;

public class AccountController {

    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    public Handler createAccount = ctx -> {
        CreateAccountJson accountJson = ctx.bodyAsClass(CreateAccountJson.class);
        Account account = accountService.createAccount(accountJson);
        ctx.json(account);
        ctx.status(201);
    };
}
