package com.hans.bankito.account.domain.model;

import com.google.common.util.concurrent.AtomicDouble;
import com.hans.bankito.exception.InsufficientBalance;

public class Account {

    private Integer id;
    private AtomicDouble amount;

    public Account(Integer id) {
        this.id = id;
    }

    public Account(Double amount) {
        this.amount = new AtomicDouble();
        this.amount.set(amount);
    }

    public Account(Integer id, Double amount) {
        this.amount = new AtomicDouble();
        this.id = id;
        this.amount.set(amount);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return (amount != null) ? amount.get() : 0.0;
    }

    public void deposit(Double amount) {
        this.amount.getAndAdd(amount);
    }

    public void withdrawal(Double amount) throws InsufficientBalance {
        if (this.amount.get() < amount) throw new InsufficientBalance();
        this.amount.getAndSet(this.getAmount() - amount);
    }
}
