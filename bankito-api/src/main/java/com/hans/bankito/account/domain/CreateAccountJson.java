package com.hans.bankito.account.domain;

public class CreateAccountJson {

    public CreateAccountJson() {
    }

    public CreateAccountJson(Double amount) {
        this.amount = amount;
    }

    private Double amount;

    public Double getAmount() {
        return amount;
    }
}
