package com.hans.bankito.account;

import com.hans.bankito.account.domain.model.Account;
import com.hans.bankito.account.domain.CreateAccountJson;
import com.hans.bankito.exception.NotFoundAccountException;

public class AccountService {

    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account createAccount(CreateAccountJson accountJson) {
        Account account = new Account(accountJson.getAmount());
        return accountRepository.save(account);
    }

    public Account getAccount(Integer accountId) throws NotFoundAccountException {
        return accountRepository.find(accountId);
    }
}
