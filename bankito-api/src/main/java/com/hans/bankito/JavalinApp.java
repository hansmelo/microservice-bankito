package com.hans.bankito;

import com.hans.bankito.account.AccountController;
import com.hans.bankito.account.AccountRepository;
import com.hans.bankito.account.AccountService;
import com.hans.bankito.transaction.TransactionController;
import com.hans.bankito.transaction.TransactionService;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {

    private int port;

    private AccountService accountService;
    private TransactionService transactionService;
    private AccountController accountController;
    private TransactionController transactionController;

    public JavalinApp(int port) {
        this.port = port;
        this.accountService = new AccountService(AccountRepository.getInstance());
        this.accountController = new AccountController(accountService);
        this.transactionService = new TransactionService(accountService);
        this.transactionController = new TransactionController(transactionService);
    }

    public Javalin init() {
        Javalin app = Javalin.create()
                .port(port)
                .exception(Exception.class, (e, ctx) -> e.printStackTrace())
                .start();

        app.routes(() -> {
            get("/bankito-api/alive", ctx -> ctx.result("I am alive!"));
            post("/bankito-api/account", accountController.createAccount);
            post("/bankito-api/transfer", transactionController.transfer);
        });

        return app;
    }
}
