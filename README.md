## Bankito

Bankito cover several topics: 

- Java8
- Javalin
- Unirest
- Concurrency
- Thread-safe
- Multi-thread
- TDD
- Junit
- Mockito
- Monitoring
- Prometheus
- Grafana
- Docker
- Docker Compose
- Cloud hosting - DigitalOcean - http://monsterend.com

## usage

To run tests:

```shell
$ mvn test
```

To run this service:

```shell
$ mvn clean install
$ java - jar target/bankito-api-1.0.0-jar-with-dependencies.jar
```

To build these services:

```shell
$ sudo docker-compose build & sudo docker-compose up -d
```


## Sample requests

Get alive 
```shell
$ curl -v "http://monsterend.com/bankito-api/alive"
```
```response
* Trying 159.89.14.68...
* TCP_NODELAY set
* Connected to monsterend.com (159.89.14.68) port 80 (#0)
> GET /bankito-api/alive HTTP/1.1
> Host: monsterend.com
> User-Agent: curl/7.54.0
> accept: application/json
>
< HTTP/1.1 200 OK
< Date: Tue, 22 Jan 2019 22:56:21 GMT
< Server: Javalin
< Content-Type: text/plain
< Content-Length: 11
<
* Connection #0 to host monsterend.com left intact
I am alive!
```

Create account
```shell
$  curl -v -H "Content-Type: application/json" -XPOST -d '{ "amount": "300.0"}' http://monsterend.com/bankito-api/account
```
```response
* Trying 159.89.14.68...
* TCP_NODELAY set
* Connected to monsterend.com (159.89.14.68) port 80 (#0)
> POST /bankito-api/account HTTP/1.1
> Host: monsterend.com
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 20
>
* upload completely sent off: 20 out of 20 bytes
< HTTP/1.1 201 Created
< Date: Tue, 22 Jan 2019 22:59:52 GMT
< Server: Javalin
< Content-Type: application/json
< Content-Length: 23
<
* Connection #0 to host monsterend.com left intact
```
```json
{
    "id":1,
    "amount":300.0
}
```
Transfer money
```shell
$  curl -v -H "Content-Type: application/json" -XPOST -d '{ "senderAccountId": "1", "reiceverAccountId": "2", "amount": "100.0"}' http://monsterend.com/bankito-api/transfer
```
```response
* Trying 159.89.14.68...
* TCP_NODELAY set
* Connected to monsterend.com (159.89.14.68) port 80 (#0)
> POST /bankito-api/transfer HTTP/1.1
> Host: monsterend.com
> User-Agent: curl/7.54.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 70
>
* upload completely sent off: 70 out of 70 bytes
< HTTP/1.1 201 Created
< Date: Tue, 22 Jan 2019 23:04:03 GMT
< Server: Javalin
< Content-Type: application/json
< Content-Length: 198
<
* Connection #0 to host monsterend.com left intact
```
```json
{
    "id":"ec76d7aa-25ca-497a-a73e-3f6caa7a2c3b",
    "senderAccount":{"id":1,"amount":200.0},
    "receiverAccount":{"id":2,"amount":700.0},
    "valueOperation":100.0,
    "status":"SUCCESS",
    "msg":"Transaction is done!"
}
```

## Monitoring
Grafana: http://monsterend.com:3000

User: admin
Pass:admin

[Graphs from docker containers](http://monsterend.com:3000/dashboard/db/docker-containers?refresh=10s&orgId=1)

[Graphs from docker host](http://monsterend.com:3000/dashboard/db/docker-host?refresh=10s&orgId=1)

[Graphs from monitor service](http://monsterend.com:3000/dashboard/db/monitor-services?refresh=10s&orgId=1)


## Project Structure
- [bankito-api]
	 - [src/main/java]
        - [/com.hans.bankito]
            - [/account] : Controller, Service and Repository for Account.
                - [/domain]: POJOs.
            - [/transaction] : Controller, Service and Repository for Transaction.
                - [/domain]: POJOs.
            - [/exeception] : Specialized Exceptions.
	 - [src/test/java]
        - [/com.hans.bankito]
	        - [/integration] : Integration tests.
	        - [/account] : Unit test for Account.
	        - [/transaction] : Unit tests for Transaction.
	        - [/util] : Helper to create multithreads calls.
- [grafana] - confs of dashboard and datasources, setup.sh the script of import of confs.
- [prometheus] - confs of the prometheus.
